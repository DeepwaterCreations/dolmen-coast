import dbgoutput as dbg
import entities
import events

from gamemap import GameMap

class GameWorld():
    """A class to keep track of individual game maps and their relationships with each
    other in the larger world.
    """

    def __init__(self, width, height, *args, **kwargs):
        #self._mapfeatures is a 2d list that holds things like floors and walls
        #self._entities is a list of dynamic objects, which store their own coordinates

#         self.maplist = [GameMap(self, *args, **kwargs)]
#         self.load_map(self.maplist[self.current_map_idx])

        self._current_map_idx = 0
        self._maps = [GameMap(width, height)]
        self._player = entities.Player(*self._map().get_spawn(), self.get)
        self._maps[self._current_map_idx].move_entity_here(self._player)

        # dbg.add_string(

        # events.listen_to_event("on_entity_death", lambda e: self._entities.remove(e))
        # events.listen_to_event("world_remove_entity", lambda e: self._entities.remove(e))
        # events.listen_to_event("world_add_entity", lambda e: self._entities.append(e))
        # events.listen_to_event("change_map", self.change_map)
        # events.listen_to_event("change_map_down", self.change_map_down)
        # events.listen_to_event("change_map_up", self.change_map_up)

    def update(self):
        self._map().update()

    def _map(self):
        #Convenience function
        return self._maps[self._current_map_idx]

    def get(self, x, y):
        """Returns the contents of the cell at x, y as a (mapfeatures, entities) tuple"""
        if (x < 0 or x > self._map().width-1) or (y < 0 or y > self._map().height-1):
            return (mapfeatures.Void(), [])
        cell_entities = list(filter(lambda e: e.x == x and e.y == y, self._map()._entities))
        return (self._map().get(x, y), cell_entities)

    def get_map_array(self):
        return self._map().get_tile_array()

    def get_local_entities(self):
        #Entities from the current map
        #as opposed to entities from across all maps
        #Currently returns a shallow copy - is that bad?
        return self._map().get_entities()
