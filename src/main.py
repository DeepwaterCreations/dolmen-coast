#!/usr/bin/env python3

import sys
import curses

import dbgoutput
import keyinput
import events
from tile import define_basic_tiles
from gamemap import GameMap
from gameworld import GameWorld
from screenpanels import MessagePanel, ListMenu, GamePanel

def main(stdscr):
    #Initialize curses
    curses.curs_set(False) #Turn off the cursor
    #Initialize debug output printer
    dbgoutput.init(stdscr)
    #Clear the terminal
    stdscr.clear()

    #Create some simple tiles
    #TODO: Make this better
    define_basic_tiles()    

    #Create a new map to fill the screen.
    #Note that GameWorld's size is the size of the initial map,
    #not the size of the view. GamePanel handles the view size.
    world = GameWorld(curses.COLS, curses.LINES-1)

    gamepanel, panellist = create_panel_layout(stdscr)

    #Output debugging messages in the upper-left corner
    dbgoutput.print_output()

    #Game Loop
    while True:
        try:
            draw_screen(stdscr, world, gamepanel, panellist, show_debug_text=True)
            keyinput.handle_key(stdscr.getkey())
            world.update()
        except KeyboardInterrupt:
            #Ctrl-C
            stdscr.refresh()
            sys.exit()
        except SystemExit:
            stdscr.refresh()
            sys.exit()

    #Close curses and put the terminal back in normal mode.
    stdscr.refresh()
    stdscr.getkey()

def draw_screen(stdscr, world, gamepanel, panellist, show_debug_text=False):
    #Update panels
    for panel in panellist:
        panel.display()

    gamepanel.display(world.get_map_array(), world.get_local_entities())

    if show_debug_text:
        dbgoutput.print_output()

def create_panel_layout(stdscr):
    """Returns a tuple:
    First, the game window.
    Second, a list of other game panels
    These are all sub-windows of stdscr.
    """
    screen_width = curses.COLS-1
    screen_height = curses.LINES-1

    MESSAGEPANEL_HEIGHT = 5
    gamepanel_width = 3 * (screen_width // 4)

    #Args are height, width, top, left
    messagepanel = MessagePanel(stdscr.subwin(MESSAGEPANEL_HEIGHT, gamepanel_width, 0, 0))
    gamepanel = GamePanel(stdscr.subwin(screen_height - MESSAGEPANEL_HEIGHT, gamepanel_width, MESSAGEPANEL_HEIGHT + 1, 0))
    menupanel = ListMenu(stdscr.subwin(screen_height, (screen_width // 4), 0, gamepanel_width + 1))

    return (gamepanel, [messagepanel, menupanel])



if __name__ == "__main__":
    #Wrap our program in a curses scope.
    curses.wrapper(main)
    #This will clean up the terminal state if the program throws an exception,
    #or just after it finishes running.
